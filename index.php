<?php

echo "<html>\n";
echo "    <head>    \n";
echo "        <title>Personal Website</title>\n";
echo "        <link rel=\"stylesheet\" href=\"style.css\">\n";
echo "        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\" integrity=\"sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\" crossorigin=\"anonymous\">\n";
echo "    </head>\n";
echo "\n";
echo "    <body>\n";
echo "    <div class=\"prof-box\">\n";
echo "        <img src=\"profile-pic.jpg\" class=\"prof-pic\">\n";
echo "            <div class=\"prof-description\">\n";
echo "                <br><h1>Alvin Cabusora</h1>\n";
echo "                <h4>A Half Baked Newbie <abbr title=\"Developer\">Dev</abbr></h4>\n";
echo "            </div>\n";
echo "                <p>\n";
echo "                    <q> My passion is to achieve my goals in life within my abilities. </q>\n";
echo "                </p>\n";
echo "            <div class=\"view-resume\">\n";
echo "                <br>\n";
echo "                <a href=\"index1.php\"  title=\"View Alvin's Resume\"><button type=\"button\">View Resume</button></a>\n";
echo "            </div>   \n";
echo "            <br>\n";
echo "            <div class=\"facebook\">\n";
echo "                    <a href=\"https://www.facebook.com/as.cabusora\" target=\"_blank\"><i class=\"fab fa-facebook-f\" title=\"Facebook\"> :  Facebook</i></a><br><br>\n";
echo "            </div>\n";
echo "            <div class=\"twitter\">\n";
echo "                    <a href=\"https://www.twitter.com\" target=\"_blank\"><i class=\"fab fa-twitter\" title=\"Twitter\"> :  Twitter</i></a><br><br>\n";
echo "            </div>\n";
echo "            <div class=\"google\">\n";
echo "                    <a href=\"https://plus.google.com/u/0/115764438295867659190\" target=\"_blank\"><i class=\"fab fa-google-plus-g\"title=\"Google+\"> :  Google+</i></a><br><br>\n";
echo "            </div>\n";
echo "            <div class=\"gitlab\">\n";
echo "                    <a href=\"https://gitlab.com/Vinzter\" target=\"_blank\"><i class=\"fab fa-gitlab\" title=\"GitLab\"> :  GitLab</i></a>\n";
echo "            </div> \n";
echo "    </div>                        \n";
echo "    </body>\n";
echo "\n";
echo "\n";
echo "\n";
echo "\n";
echo "</html>";

?>

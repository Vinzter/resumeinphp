<?php

echo "<html>\n";
echo "    <head>\n";
echo "        <title>Resume</title>\n";
echo "        <link rel=\"stylesheet\" href=\"style1.css\">\n";
echo "        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\" integrity=\"sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\" crossorigin=\"anonymous\">\n";
echo "    </head>\n";
echo "    <body>\n";
echo "        <div class=\"right-box\">\n";
echo "                <img src=\"profile-pic.jpg\" class=\"prof-pic\">\n";
echo "            <div class=\"content\">\n";
echo "                <h1>Alvin S. Cabusora</h1>\n";
echo "                <h4><u>Newbie Developer</u></h4>\n";
echo "            </div>\n";
echo "                <br><a href=\"index.php\" title=\"Return to the mainpage\"> <i class=\"fas fa-backward\"></i> <strong>BACK</strong></a>\n";
echo "        </div>\n";
echo "        <div class=\"information\">\n";
echo "            <div class=\"content\">\n";
echo "                <h1>Alvin Salangron Cabusora</h1>\n";
echo "                <h2>Newbie Developer</h2>\n";
echo "            </div>\n";
echo "        <div class=\"job-description\">\n";
echo "            <p>\n";
echo "                    <br>The role is responsible for designing, coding and modifying websites, from \n";
echo "                    layout to function and according to a client's specifications. Strive to create \n";
echo "                    visually appealing sites that feature user-friendly design and clear navigation.<hr>\n";
echo "                    <br>\n";
echo "            </p>\n";
echo "        </div>\n";
echo "            <h3><i class=\"fas fa-user\"></i>  Profile</h3><hr>\n";
echo "                <p>      <q>Detail-oriented IT professional with ten years of experience as\n";
echo "                    a software support specialist and systems/network technician. Skilled at operating in a\n";
echo "                    wide range of platforms. Excellent written and oral communication skills; \n";
echo "                    capable of explaining complex software issues in easy-to-understand terms.</q></p>\n";
echo "            <h3><i class=\"fas fa-info-circle\"></i>  Personal Data</h3><hr>\n";
echo "                <ul>\n";
echo "                        <li><strong>Birthdate</strong>        :  August 03, 1998<br></li>\n";
echo "                        <li><strong>Adress</strong>         \n";
echo "                          :  Km.7 Lanang Davao City<br></li>\n";
echo "                        <li><strong>Place of Birth</strong> :  Davao City<br></li>\n";
echo "                        <li><strong>Civil Status</strong>     :  Single<br></li>\n";
echo "                        <li><strong>Height</strong>       \n";
echo "                            :  5.0\"<br></li>\n";
echo "                        <li><strong>Weight</strong>      \n";
echo "                            :  50kgs<br></li>\n";
echo "                </ul>\n";
echo "            <h3><i class=\"fas fa-briefcase\"></i>  Educational Background</h3><hr>\n";
echo "                <ul>\n";
echo "                        <li><strong>Primary</strong>         : Francisco Bangoy Central Elementary School <br></li>\n";
echo "                        <li><strong>Secondary</strong>    : Francisco Bangoy National High School <br></li>\n";
echo "                        <li><strong>Tertiary</strong>         : University of Southeastern Philippines <br></li>\n";
echo "                        <li><strong>Course</strong>         : Bachelor of Science in Information Technology <br></li>\n";
echo "                </ul>\n";
echo "            <h3><i class=\"fas fa-lightbulb\"></i>  Professional Skills</h3><hr>\n";
echo "                <ul>\n";
echo "                    <li><b>Programming</b></li>\n";
echo "                    <li><b>Photo Editing</b></li>\n";
echo "                    <li><b>Video Editing</b></li>                    \n";
echo "                </ul>\n";
echo "\n";
echo "        </div>\n";
echo "\n";
echo "        \n";
echo "    </body>\n";
echo "</html>";

?>
